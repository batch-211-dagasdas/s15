console.log("hello world");

//JS renders web pages in an interactive and dynamic fashion, meaning it enable us to create dynamically updating content, control multimedia and animate images


//Syntax,statement, and comments

//statement:

//statment in programming are intruction that we tell the compudter to perform
//js statement usually end w/ a semicolon(;)
//semicolon are not required in js but we will use it to help us train to locate where a statement ends

//syntax in programming, it is the set of rules that descrives how statement must be constructed.

// comments are part of the code that gets ignored by the language
// comments are meant to describe the written code

/*
 there are 2 type of comment: 
 1. the single-line comment (ctril + /) 
 2.the multi-line comment (ctrl + shft + /)
 ** denoted by a slash and asterisk
*/

//variables

//it is used to contain data
// this make is easier for us to associate information stored in our devices to actual "names " about information

//Declaring variable - tells our devices that a variable name is to created and is ready to store data.

let myVariable = "me";
console.log(myVariable)

//Decliring a variable w/out giving is a value will automatically assign it with the value of u"undifined", meaning the variable's value was not defined

//syntax
	// let / const variableName;

// console.log() is usefull for printing values of variables or certain results of code into google chrome browser console
//constant use of this throughout developing an application will save us time and build good habits in alwats checking for output of our code.

let hello;
console.log(hello); // undifined

// variable must be declared 1st before used
// using variable before declared wil return an error

/*
	Guides in Writing variables
	1- Use the "let " Keyword ff by the variable name of ur choosing and use the assignment operator (=) to assign a value

	2- Variable names should start with a lowercase character use camelCase for multiple words

	3-for constant variable, use the "const" keyword
	4-variable name should be indicative (or descriptive) of the value being stored to avoid confusion
*/

// declare and initialize variable
// initializing var- the instance when a var is given its initia; or startin value

// syntax
	// let/const variableName=value;

	let productNAme ='desktop computer';
	console.log(productNAme)

	let productPrice = 18999;
	console.log(productPrice);

	const interest = 3.539;

// reassigning var values means changing its initial or previous val into anothe val

// syntax
	// variableName=newValue
	productNAme ='laptop'
	console.log(productNAme)

	// // let var connot be re-declared within its scope so this will work
	// let friend ='kate';
	// let friend ='nej';
	// console.log(friend)

// reassigning variables vs initializing var
// declared a var 1st

// let supplier;
// supplier ="john smith trading"
// initialization is done after the var has been declared
// this is considered as initialization bc it is the 1st time that a value have been assigned to a var
// console.log(supplier)

// supplier="zuit Store";
// console.log(supplier)

//  connot declare a const w/out initialization
// const pi;
// pi ='3.1416';
// console.log(pi);

// var
// var is also use in declaring a variable
// let / const they were introduceds as new feature

// a= 5;
// console.log(a);
// var a
// hoisting is jscript defaulf behavior of moving declaration to the top
// in term of variable and constants, keyword var is hoisted and let and const does not allow hoisting

// a=5;
// console.log(a);
// let a

// let /const local/global scope
	// scope essentaily means where variable are available for use
	// let and const are block-scope
	// block is a chuck of code bounded by {}. a block live in curly braces, anything within curly braces is a block
	// so a variable declared in a block w/ let is only available for use w/in that block


	// let outerVariable ="hello"
	// {
	// 	let innerVariable ="hello again";
	// }

	// console.log(outerVariable);
	// console.log(innerVariable);

	// multiple variable declarations
	// mutilple variable may be declared in one line 
	// though it is quicker to do w.out having to retype the "let" keyword, it is still best practice to use multiple "let /const" keywork when declaring variables
	// using multiple keywords make code easier to read and determane whiat kind of variable has been created

	let productCode = 'DC017'
	const productbrand ='dell'
	console.log(productCode, productbrand)


	// data types

	// string are a series of characters that create a word, phrase, a sentence or anything related to creating text
	// string in js can be writen using either a single ('') or a dd("") quote
	// in other prog lang , only dd quotes ("") can be used for creating strings

	let country ='philippines'
	let province= ' bislig'

	// concatenating string
	// muliple string values can be combined to create asingle string using the '+' symbol

	let fullAddress= province+ ',' + country;
	console.log(fullAddress);


	let greeting ="i live in the " + country
	console.log(greeting)

	// the escape character (/) in strings in combination w/ other characters can produce deff effects
	//  "\n" refers to creating a new line in between text

	let mailAddress= 'metro manila \n\n philippines'
	console.log(mailAddress)

	// using the "" along w/ '' can allow us to easily include single quotes in text w/out using the escape character

	let message ="John's employees went home early"
	console.log(message);
	message = 'John\'s employees went home early'
	console.log(message)


	// numbers
	// Integers/ whole numbers

	let headcount = 26;
	console.log(headcount);

	// decimal/ fractions
	// let grade =98.7;
	// console.log(grade);

	// exponential notation
	let planetDistance = 2e10;
	console.log(planetDistance)

	// // combining text and strings 
	// console.log("John's grade last quarter is " + grade)

	// boolean
	// boolean values are normally use to store values relating to the state of certain things 
	// this will be useful in further discussions about creating login to make our application respond to certain situation/ scenarios

	let isMarried= false;
	let inGoodConduct =true;

	console.log("is Married:" + isMarried);
	console.log("isGoodConduct:" + inGoodConduct);

	// arrays are a special kind of data type that is use to store multiple values
	// arrays can store diff data type but is normally used to store similar data types

	// syntax
		// let /const arrayName=[elementA, elementV]
	// array literals

	let grades = [98.7,8,656,56,56];
	console.log(grades)


	// object
		// let /const objectname={ propertyA: value,propertyb: value}

	let person = {
		fullName: 'ralph Dagasdas',
		age: 25,
		isMarried: false,
		contact: ["09154854645","8132 4575"],
		address: {
			houseNumber:'4556',
			city: 'bislig'
		}
	}

	console.log(person)




